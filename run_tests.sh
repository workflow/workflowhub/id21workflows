#!/bin/bash

set -e

SCRIPT_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

START_TIME=$(date +%s)

function report_runtime {
    END_TIME=$(date +%s)
    ELAPSED_TIME=$(( END_TIME - START_TIME ))
    echo
    echo "Total runtime: ${ELAPSED_TIME} seconds"
}

trap report_runtime EXIT

if [[ -n "${SLURM_JOBID}" ]]; then
    TMP_ROOT="/tmp_14_days/$(whoami)/ewoksdemo/slurm_job_${SLURM_JOBID}"
else
    TMP_ROOT="/tmp/ewoksdemo"
fi

for filename in "${SCRIPT_ROOT}"/tests/*.py; do
    rm -rf "${TMP_ROOT}"

    echo
    echo "TEST ${filename}"
    python "${filename}"
done

rm -rf "${TMP_ROOT}"

for filename in "${SCRIPT_ROOT}"/scripts/*.py; do
    echo
    echo "SCRIPT ${filename}"
    python "${filename}"
done
