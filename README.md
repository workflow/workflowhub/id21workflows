# XRF data processing with Ewoks

## Usage

### Download files

At the start of the experiment, clone the ID21 workflows from the hub to the proposal SCRIPTS directory

```bash
cd /data/visitor/<expnumber>/id21/<date>/SCRIPTS
git clone https://gitlab.esrf.fr/workflow/workflowhub/id21workflows.git
cd id21workflows
```

### Run workflows

Activate the correct python environment

```bash
module load ewoksid21
```

Data processing with a script

```bash
python ./scripts/xrfmap_single_detector.py
```

The scripts are

* `xrfmap.py`: single XRF map
* `mosaic_xrfmap.py`: mosaic XRF map
* `fluoxas.py`: fluoXAS stack (can be SIXES as well)

Interactive data processing (starting Orange can take some time)

```bash
ewoks-canvas ./workflows/xrfmap_single_detector.ows
```

The [ewoksfluo documentation](https://ewoksfluo.readthedocs.io) provides more
details about the workflows and their parameters.

### Reproducability

Parameterized workflows are saved with the results.

Install add dependencies that were install when the workflow was executed

```bash
ewoks install .../PROCESSED_DATA/.../DIV15_10uM_2_roi75698_90925_scan0001.json
```

Re-run the workflow

```bash
ewoks execute .../PROCESSED_DATA/.../DIV15_10uM_2_roi75698_90925_scan0001.json
```

## Installation outside the ESRF

On any computer you can execute these commands to install `ewoksfluo`
Note: if you want to install `orange3` just remove the square brackets

```bash
python3 -m venv id21env
source id21env/bin/activate
pip install -U pip setuptools
```

If you want to run the scripts without Orange

```bash
pip install ewoksfluo pyqt5
```

If you want to run the scripts with Orange

```bash
pip install ewoksfluo pyqt5 orange3
```

The first three commands are to create on isolated python enviroment in a local
directory with a new of your choice ("id21env" in the example).

## Sum detectors vs. sum results

Process all data in `/data/visitor/ls3288/id21/20240221/RAW_DATA/DIV15_10uM_2/DIV15_10uM_2_roi75698_90925/DIV15_10uM_2_roi75698_90925.h5`
(this is restored data so we cannot write in `PROCESSED_DATA`)

```bash
python ./scripts/xrfmap_multi_detector.py
```

```bash
python ./scripts/xrfmap_multi_detector_sumspectra.py
```

Compare results.

| Si_K[0] norm      |   Sum results |   Sum spectra | Comment                          | Correction        |
|-------------------|---------------|---------------|----------------------------------|-------------------|
| peak area         |    15512.7    |    15511.8    |                                  |                   |
| peak area stddev  |       14.0086 |        5.6405 | fitweight=0 (otherwise the same) |                   |
| mass fraction (%) |       27.0954 |      128.352  | Solid angle: x NDET              | 25.67038672372619 |

Note: `fitweight=2` would show peak area stddev ~112 the same for both and ~21.7% mass fraction ???
