import re
import h5py
import numpy
from pprint import pprint
from tabulate import tabulate

ELEMENT = "Si_K"


def data_from_fit(filename, scan, group):
    with h5py.File(filename) as nxroot:
        fit = nxroot[f"/{scan}/fit"]
        data = dict()
        for name in fit:
            data[name] = fit[f"{name}/results/{group}/{ELEMENT}"][0]
        return data


def data_from_nxprocess(filename, scan, process):
    with h5py.File(filename) as nxroot:
        results = nxroot[f"/{scan}/{process}/results"]
        data = dict()
        for name in results:
            data[name] = results[f"{name}/{ELEMENT}"][0]
        return data


def data_from_raw(filename, scan, pattern):
    with h5py.File(filename) as nxroot:
        measurement = nxroot[f"/{scan}/measurement"]
        data = dict()
        for name in measurement:
            if re.match(pattern, name):
                data[name] = measurement[name][0]
        return data


def compare_sum_before_and_after(bliss_file, output_file, output_file_sumspectra, scan):
    live_time = data_from_raw(bliss_file, scan, "^.+live_time$")
    counters = data_from_raw(bliss_file, scan, "^.+iodet$")
    assert len(counters) == 1
    iodet = list(counters.values())[0]
    parameters = data_from_fit(output_file, scan, "parameters")
    massfractions = data_from_fit(output_file, scan, "massfractions")
    uncertainties = data_from_fit(output_file, scan, "uncertainties")
    norm = data_from_nxprocess(output_file, scan, "norm")

    livetime_ref_value = 0.050
    iodet_ref_value = 75000
    detector_weights = {
        det: livetime_ref_value / value for det, value in live_time.items()
    }

    # https://ewoksfluo.readthedocs.io/en/latest/howtoguides/add_detectors.html

    parameters_final = 0
    for det, value in parameters.items():
        parameters_final += value * detector_weights[f"{det}_live_time"]
    parameters_final_norm = parameters_final * iodet_ref_value / iodet
    numpy.testing.assert_almost_equal(parameters_final_norm, norm["parameters"])

    uncertainties_final = 0
    for det, value in uncertainties.items():
        uncertainties_final += value**2 * detector_weights[f"{det}_live_time"] ** 2
    uncertainties_final **= 0.5
    uncertainties_final_norm = uncertainties_final * iodet_ref_value / iodet
    numpy.testing.assert_almost_equal(uncertainties_final_norm, norm["uncertainties"])

    massfractions_final = 0
    for det, value in massfractions.items():
        massfractions_final += (
            value
            * detector_weights[f"{det}_live_time"]
            * parameters[det]
            * detector_weights[f"{det}_live_time"]
        )
    massfractions_final /= parameters_final
    massfractions_final_norm = massfractions_final * iodet_ref_value / iodet
    numpy.testing.assert_almost_equal(massfractions_final_norm, norm["massfractions"])

    parameters2 = data_from_fit(output_file_sumspectra, scan, "parameters")
    _ = data_from_fit(output_file_sumspectra, scan, "massfractions")
    uncertainties2 = data_from_fit(output_file_sumspectra, scan, "uncertainties")
    norm2 = data_from_nxprocess(output_file_sumspectra, scan, "norm")

    ndetectors = len(massfractions)
    table = [
        [
            "peak area",
            norm["parameters"],
            norm2["parameters"],
            "",
            "",
        ],
        [
            "peak area stddev",
            norm["uncertainties"],
            norm2["uncertainties"],
            "fitweight=0 (otherwise the same)",
            "",
        ],
        [
            "mass fraction (%)",
            norm["massfractions"] * 100,
            norm2["massfractions"] * 100,
            "Solid angle: x NDET",
            norm2["massfractions"] * 100 / ndetectors,
        ],
    ]
    print(
        tabulate(
            table,
            headers=[
                f"{ELEMENT}[0] norm",
                "Sum results",
                "Sum spectra",
                "Comment",
                "Correction",
            ],
            tablefmt="github",
        )
    )

    pprint(parameters)
    pprint(parameters2)
    pprint(uncertainties)
    pprint(uncertainties2)


def calc_uncertainty(area, sigma):
    # Assume Gaussian peak shape and Poisson statistics
    #
    # Error of the sum is equal to sum or the errors
    # print(numpy.sqrt(5*calc_uncertainty(3000, 0.040)**2))
    # print(numpy.sqrt(calc_uncertainty(5*3000, 0.040)**2))
    var_area = area
    var_sigma = 0
    return numpy.sqrt(2 * numpy.pi * (sigma**2 * var_area + area**2 * var_sigma))


if __name__ == "__main__":
    bliss_file = "/data/visitor/ls3288/id21/20240221/RAW_DATA/DIV15_10uM_2/DIV15_10uM_2_roi75698_90925/DIV15_10uM_2_roi75698_90925.h5"
    output_file = "/tmp/ls3288/ewoksdemo2/DIV15_10uM_2_roi75698_90925.h5"
    output_file_sumspectra = "/tmp/ls3288/ewoksdemo3/DIV15_10uM_2_roi75698_90925.h5"
    scan = "1.1"
    compare_sum_before_and_after(bliss_file, output_file, output_file_sumspectra, scan)
