import os
from datetime import datetime
from datetime import timedelta
from glob import glob

import h5py


def detect_scanned_motors(nxentry):
    scanned = set()
    try:
        motor_names = list(nxentry["instrument/positioners_start"])
    except KeyError:
        return scanned
    for motor_name in motor_names:
        try:
            dset = nxentry[f"instrument/{motor_name}/value"]
        except KeyError:
            try:
                dset = nxentry[f"instrument/{motor_name}/data"]
            except KeyError:
                continue
        if dset.size == 1:
            continue
        units = dset.attrs.get("units", None)
        scanned.add(f"{motor_name}(size={dset.size}, units={units})")
    return scanned


def detect_scanned_mcas(nxentry):
    mcas = set()
    try:
        nxentry["instrument"]
    except KeyError:
        return mcas
    for name, item in nxentry["instrument"].items():
        if item.attrs.get("NX_class", None) == "NXdetector":
            try:
                if item["type"][()].decode().lower() == "mca":
                    item["live_time"]
                    mcas.add(name)
            except KeyError:
                continue
    return mcas


def detect_xrfmaps(nxentry):
    scanned = detect_scanned_motors(nxentry)
    mcas = detect_scanned_mcas(nxentry)
    if len(scanned) < 2 or len(mcas) < 2:
        return
    scanned = sorted(scanned)
    mcas = sorted(mcas)
    scan = nxentry.name
    return f" {scan=}\n  {scanned=}\n  {mcas=}"


def investigate_dataset(dataset_master):
    scans = list()
    with h5py.File(dataset_master, mode="r", locking=False) as nxroot:
        try:
            file_time = datetime.fromisoformat(nxroot.attrs["file_time"])
        except KeyError:
            return scans
        now = datetime.now().astimezone()
        if (now - file_time) > timedelta(days=100):
            return scans

        for scan, nxentry in nxroot["/"].items():
            scan = detect_xrfmaps(nxentry)
            if scan:
                scans.append(scan)
    if scans:
        print()
        print(dataset_master, file_time)
        print("\n".join(scans))


def investigate_session(session_dir):
    dataset_masters = glob(os.path.join(session_dir, "*", "*", "*.h5"))
    for dataset_master in dataset_masters:
        investigate_dataset(dataset_master)


if __name__ == "__main__":
    investigate_session("/data/visitor/md1419/id21/20240917/RAW_DATA/")
