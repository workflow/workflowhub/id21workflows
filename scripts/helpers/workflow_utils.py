import os
import getpass
import logging
from pprint import pprint
from typing import Optional, Tuple, Sequence, Union, Dict

from ewoks import execute_graph

logging.basicConfig(level=logging.INFO)


REAL_DATA_ROOT = "/data/visitor"

DEMO_DATA_ROOT = "/data/scisoft/ewoks"

DEFAULT_OUT_DIRNAME = "ewoks_results"

AXES_UNITS = {
    "sxm": {
        "samy": "mm",
        "samz": "mm",
        "sampy": "um",
        "sampz": "um",
    },
    "nano": {
        "nsy": "mm",
        "nsz": "mm",
        "nspy": "um",
        "nspz": "um",
    },
}

VIRTUAL_AXES = {
    "sxm": {
        "sy": "<samy>+<sampy>",
        "sz": "<samz>+<sampz>",
    },
    "nano": {
        "sy": "<nsy>+<nspy>",
        "sz": "<nsz>+<nspz>",
    },
}

IGNORE_AXES = {
    "sxm": [],
    "nano": ["nsz1", "nsz2", "nsz3"],
}

ENERGY_COUNTER = {"sxm": "Edcm", "nano": "Edcm"}

I0_COUNTER = {"sxm": "iodet", "nano": "niodet"}

MCA_NAME_FORMAT = {"sxm": "fx2_det{}", "nano": "fx_nano_det{}"}

DEFAULT_LIVETIME_REF_VALUE = "0.1"

DEFAULT_COUNTER_REF_VALUE = "np.mean(<instrument/{}/data>)"


def _normalize_path(path: str, *parts) -> str:
    if parts:
        path = os.path.join(path, *parts)
    path = os.path.abspath(path)
    path = path.replace("/mnt/multipath-shares", "")
    return path


WORKFLOW_DIR = _normalize_path(os.path.dirname(__file__), "..", "..", "workflows")


def xrfmap(
    session: str,
    sample: str,
    dataset: str,
    scan_number: str,
    config_filenames: Sequence[str],
    detector_numbers: Sequence[int],
    nano: bool,
    livetime_ref_value: Union[str, int, float, None] = None,
    counter_ref_value: Union[str, int, float, None] = None,
    counter_name: Optional[str] = None,
    energy_name: Optional[str] = None,
    dirname: str = DEFAULT_OUT_DIRNAME,
):
    filename = _raw_directory(session, sample, dataset)

    output_filename = _processed_path(
        session, sample, dataset, dirname, f"{sample}_{dataset}.h5"
    )
    convert_destination = _processed_path(
        session,
        sample,
        dataset,
        dirname,
        f"{sample}_{dataset}_scan{scan_number:04d}.json",
    )

    output_root_uri = f"{output_filename}::/{scan_number}.1"
    config_filenames = [_pymca_config_path(session, s) for s in config_filenames]

    inputs, sum_spectra = _common_parameters(
        detector_numbers,
        config_filenames,
        energy_name,
        counter_name,
        nano,
        livetime_ref_value=livetime_ref_value,
        counter_ref_value=counter_ref_value,
    )

    inputs += [
        {
            "name": "filename",
            "value": filename,
            "all": True,
        },
        {
            "name": "scan_number",
            "value": scan_number,
            "all": True,
        },
        {
            "name": "output_root_uri",
            "value": output_root_uri,
            "all": True,
        },
        {
            "name": "axes_units",
            "value": AXES_UNITS["nano" if nano else "sxm"],
            "all": True,
        },
        {
            "name": "ignore_positioners",
            "value": IGNORE_AXES["nano" if nano else "sxm"],
            "all": True,
        },
    ]

    if sum_spectra is None:
        workflow = "xrfmap_single_detector.ows"
    elif sum_spectra:
        workflow = "xrfmap_multi_detector_sumspectra.ows"
    else:
        workflow = "xrfmap_multi_detector.ows"
    workflow = os.path.join(WORKFLOW_DIR, workflow)

    print()
    result = execute_graph(
        workflow, inputs=inputs, convert_destination=convert_destination
    )
    pprint(result)


def mosaic_xrfmap(
    session: str,
    sample: str,
    datasets: Sequence[str],
    scan_ranges: Sequence[Tuple[int, int]],
    config_filenames: Sequence[str],
    detector_numbers: Sequence[int],
    nano: bool,
    livetime_ref_value: Union[str, int, float, None] = None,
    counter_ref_value: Union[str, int, float, None] = None,
    counter_name: Optional[str] = None,
    energy_name: Optional[str] = None,
    dirname: str = DEFAULT_OUT_DIRNAME,
    exclude_scans: Optional[Sequence[Sequence[int]]] = None,
    resolution: Optional[Dict[str, Tuple[Union[int, float], str]]] = None,
):
    filenames = [_raw_directory(session, sample, dataset) for dataset in datasets]

    if len(filenames) != len(scan_ranges):
        raise ValueError(
            f"{len(filenames)} scan ranges (first, last) are needed for {len(filenames)} files"
        )

    if not exclude_scans:
        exclude_scans = [()] * len(filenames)

    if len(filenames) != len(exclude_scans):
        raise ValueError(
            f"{len(filenames)} exclude scan list are needed for {len(filenames)} files"
        )

    first_dataset = datasets[0]
    first_scan_number = scan_ranges[0][0]

    concat_filename = _processed_path(
        session, sample, first_dataset, dirname, f"{sample}_{first_dataset}_concat.h5"
    )
    output_filename = _processed_path(
        session, sample, first_dataset, dirname, f"{sample}_{first_dataset}_results.h5"
    )
    convert_destination = _processed_path(
        session,
        sample,
        first_dataset,
        dirname,
        f"{sample}_{first_dataset}_scan{first_scan_number:04d}.json",
    )

    concat_bliss_scan_uri = f"{concat_filename}::/{first_scan_number}.1"
    output_root_uri = f"{output_filename}::/{first_scan_number}.1"
    config_filenames = [_pymca_config_path(session, s) for s in config_filenames]

    inputs, sum_spectra = _common_parameters(
        detector_numbers,
        config_filenames,
        energy_name,
        counter_name,
        nano,
        livetime_ref_value=livetime_ref_value,
        counter_ref_value=counter_ref_value,
    )

    inputs += [
        {
            "name": "filenames",
            "value": filenames,
            "all": True,
        },
        {
            "name": "scan_ranges",
            "value": scan_ranges,
            "all": True,
        },
        {
            "name": "exclude_scans",
            "value": exclude_scans,
            "all": True,
        },
        {
            "name": "bliss_scan_uri",
            "value": concat_bliss_scan_uri,
            "all": True,
        },
        {
            "name": "output_root_uri",
            "value": output_root_uri,
            "all": True,
        },
        {
            "name": "axes_units",
            "value": AXES_UNITS["nano" if nano else "sxm"],
            "all": True,
        },
        {
            "name": "virtual_axes",
            "value": VIRTUAL_AXES["nano" if nano else "sxm"],
            "all": True,
        },
        {
            "name": "positioners",
            "value": sorted(VIRTUAL_AXES["nano" if nano else "sxm"])[::-1],
            "all": True,
        },
        {
            "name": "resolution",
            "value": resolution,
            "all": True,
        },
    ]

    if sum_spectra is None:
        workflow = "mosaic_single_detector.ows"
    elif sum_spectra:
        workflow = "mosaic_multi_detector_sumspectra.ows"
    else:
        workflow = "mosaic_multi_detector.ows"
    workflow = os.path.join(WORKFLOW_DIR, workflow)

    print()
    result = execute_graph(
        workflow, inputs=inputs, convert_destination=convert_destination
    )
    pprint(result)


def fluoxas(
    session,
    sample,
    datasets,
    scan_ranges,
    config_filenames: Sequence[str],
    detector_numbers: Sequence[int],
    nano: bool,
    livetime_ref_value: Union[str, int, float, None] = None,
    counter_ref_value: Union[str, int, float, None] = None,
    counter_name: Optional[str] = None,
    energy_name: Optional[str] = None,
    dirname: str = DEFAULT_OUT_DIRNAME,
    exclude_scans: Optional[Sequence[Sequence[int]]] = None,
):
    filenames = [_raw_directory(session, sample, dataset) for dataset in datasets]

    if len(filenames) != len(scan_ranges):
        raise ValueError(
            f"{len(filenames)} scan ranges (first, last) are needed for {len(filenames)} files"
        )

    if not exclude_scans:
        exclude_scans = [()] * len(filenames)

    first_dataset = datasets[0]
    first_scan_number = scan_ranges[0][0]

    output_filename = _processed_path(
        session, sample, first_dataset, dirname, f"{sample}_{first_dataset}.h5"
    )
    convert_destination = _processed_path(
        session,
        sample,
        first_dataset,
        dirname,
        f"{sample}_{first_dataset}_scan{first_scan_number:04d}.json",
    )

    output_root_uri = f"{output_filename}::/{first_scan_number}.1"
    config_filenames = [_pymca_config_path(session, s) for s in config_filenames]

    inputs, sum_spectra = _common_parameters(
        detector_numbers,
        config_filenames,
        energy_name,
        counter_name,
        nano,
        livetime_ref_value=livetime_ref_value,
        counter_ref_value=counter_ref_value,
    )

    inputs += [
        {
            "name": "filenames",
            "value": filenames,
            "all": True,
        },
        {
            "name": "scan_ranges",
            "value": scan_ranges,
            "all": True,
        },
        {
            "name": "exclude_scans",
            "value": exclude_scans,
            "all": True,
        },
        {
            "name": "output_root_uri",
            "value": output_root_uri,
            "all": True,
        },
        {
            "name": "axes_units",
            "value": AXES_UNITS["nano" if nano else "sxm"],
            "all": True,
        },
        {
            "name": "ignore_positioners",
            "value": IGNORE_AXES["nano" if nano else "sxm"],
            "all": True,
        },
    ]

    if sum_spectra is None:
        workflow = "fluoxas_single_detector.ows"
    elif sum_spectra:
        raise ValueError(
            f"Sum before fit is not supported yet for FluoXAS. Provide {len(detector_numbers)} config files."
        )
    else:
        workflow = "fluoxas_multi_detector.ows"
    workflow = os.path.join(WORKFLOW_DIR, workflow)

    print()
    result = execute_graph(
        workflow, inputs=inputs, convert_destination=convert_destination
    )
    pprint(result)


def _common_parameters(
    detector_numbers: Sequence[int],
    config_filenames: Sequence[str],
    energy_name: Optional[str],
    counter_name: Optional[str],
    nano: bool,
    livetime_ref_value: Union[str, int, float, None] = None,
    counter_ref_value: Union[str, int, float, None] = None,
) -> Tuple[Sequence[dict], Optional[bool]]:

    inputs = _energy_parameters(energy_name, nano)

    inputs += _counter_parameters(
        counter_name, nano, counter_ref_value=counter_ref_value
    )

    detector_names = [
        MCA_NAME_FORMAT["nano" if nano else "sxm"].format(i) for i in detector_numbers
    ]
    detector_inputs, sum_spectra = _detector_parameters(
        detector_names, config_filenames, livetime_ref_value=livetime_ref_value
    )
    inputs += detector_inputs

    return inputs, sum_spectra


def _detector_parameters(
    detector_names: Sequence[str],
    config_filenames: Sequence[str],
    livetime_ref_value: Union[str, int, float, None] = None,
) -> Tuple[Sequence[dict], Optional[bool]]:
    """Returns list of workflow inputs and a boolean saying to sum the detectors or not."""
    if len(detector_names) == 1:
        if len(config_filenames) != 1:
            raise ValueError("Only one pymca configuration is needed for one detector")

        inputs = [
            {"name": "config", "value": config_filenames[0], "all": True},
            {"name": "detector_name", "value": detector_names[0], "all": True},
        ]

        return inputs, None

    if len(config_filenames) == 1:

        inputs = [
            {"name": "config", "value": config_filenames[0], "all": True},
            {"name": "detector_names", "value": detector_names, "all": True},
        ]

        return inputs, True

    if len(detector_names) != len(config_filenames):
        raise ValueError(
            f"{len(detector_names)} pymca configurations are needed for {len(detector_names)} detectors"
        )

    inputs = [
        {"name": "configs", "value": config_filenames, "all": True},
        {"name": "detector_names", "value": detector_names, "all": True},
        {
            "name": "detector_normalization_template",
            "value": f"{livetime_ref_value or DEFAULT_LIVETIME_REF_VALUE}/<instrument/{{}}/live_time>",
            "all": True,
        },
    ]

    return inputs, False


def _counter_parameters(
    counter_name: Optional[str],
    nano: bool,
    counter_ref_value: Union[str, int, float, None] = None,
) -> Sequence[dict]:
    inputs = [
        {
            "name": "counter_name",
            "value": counter_name or I0_COUNTER["nano" if nano else "sxm"],
            "all": True,
        },
        {
            "name": "counter_normalization_template",
            "value": f"{counter_ref_value or DEFAULT_COUNTER_REF_VALUE}/<instrument/{{}}/data>",
            "all": True,
        },
    ]
    return inputs


def _energy_parameters(
    energy_name: Optional[str],
    nano: bool,
) -> Sequence[dict]:
    inputs = [
        {
            "name": "energy_name",
            "value": energy_name or ENERGY_COUNTER["nano" if nano else "sxm"],
            "all": True,
        },
    ]
    return inputs


def _raw_directory(session: str, sample: str, dataset: str):
    path = _normalize_path(
        session, "RAW_DATA", sample, f"{sample}_{dataset}", f"{sample}_{dataset}.h5"
    )
    if path.startswith(REAL_DATA_ROOT):
        demo_path = path.replace(REAL_DATA_ROOT, DEMO_DATA_ROOT)
        if os.path.exists(demo_path):
            path = demo_path
    return path


def _pymca_config_path(session: str, name: str) -> str:
    path = _normalize_path(session, "SCRIPTS")
    if path.startswith(REAL_DATA_ROOT):
        demo_path = path.replace(REAL_DATA_ROOT, DEMO_DATA_ROOT)
        if os.path.exists(demo_path):
            path = demo_path
    return os.path.join(path, "pymca", name)


def _processed_path(
    session: str, sample: str, dataset: str, run: str, outname: str
) -> str:
    processed_path = _normalize_path(session, "PROCESSED_DATA", run, outname)

    raw_path = _raw_directory(session, sample, dataset)
    if raw_path.startswith(DEMO_DATA_ROOT):
        job_id = os.environ.get("SLURM_JOBID")
        if job_id:
            out_root = f"/tmp_14_days/{getpass.getuser()}/ewoksdemo/slurm_job_{job_id}"
        else:
            out_root = "/tmp/ewoksdemo"
        processed_path = processed_path.replace(REAL_DATA_ROOT, out_root)

    return processed_path
