import os
import sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "helpers")))

from workflow_utils import xrfmap  # noqa E402

if __name__ == "__main__":
    # To run the script type this in a terminal
    #
    #   module load ewoksid21
    #
    # By default, data is normalized to 0.1 seconds and the average I0 of the map.

    xrfmap(
        session="/data/visitor/ls3288/id21/20240221",
        sample="DIV15_10uM_2",
        dataset="roi75698_90925",
        scan_number=1,
        config_filenames=[
            "sample_10200ev_sheldon.cfg",
        ],  # single config means sum before fit
        detector_numbers=[0, 1, 2, 3, 4],
        nano=True,
        # livetime_ref_value=0.05,  # time normalization
        # counter_ref_value=73500,  # I0 normalization
        # dirname="ewoks_result", # directory name in PROCESSED_DATA
    )
