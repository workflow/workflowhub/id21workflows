import os
import sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "helpers")))

from workflow_utils import fluoxas  # noqa E402

if __name__ == "__main__":
    # To run the script type this in a terminal
    #
    #   module load ewoksid21
    #
    # By default, data is normalized to 0.1 seconds and the average I0 of the stack.

    fluoxas(
        session="/data/visitor/ma5958/id21/20250207",
        sample="BaCrO4-com-o-aged-thin",
        datasets=["roi107244_125314"],
        scan_ranges=[[2, 3]],  # first and last scan for every dataset
        exclude_scans=[[]],  # list of scans to exclude for every dataset
        config_filenames=[
            "config_nano_6_3.cfg",
            "config_nano_6_3.cfg",
        ],  # need as many configs as detectors
        detector_numbers=[0, 1],
        nano=True,
        # dirname="ewoks_result", # directory name in PROCESSED_DATA
        # livetime_ref_value=0.03,  # time normalization
        # counter_ref_value=50000,  # I0 normalization
    )
