import os
import sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "helpers")))

from workflow_utils import fluoxas  # noqa E402

if __name__ == "__main__":
    fluoxas(
        session="/data/visitor/ls3415/id21/20250219",
        sample="Sample2_2",
        datasets=["roi109038_126989"],
        scan_ranges=[[2, 122]],
        exclude_scans=[[58,59]],
        config_filenames=[
            "config_703_11.cfg",
        ],
        detector_numbers=[0],
        nano=False,
        # dirname="wdn_test", # directory name in PROCESSED_DATA
        # livetime_ref_value=0.03,  # time normalization
        # counter_ref_value=50000,  # I0 normalization
    )
