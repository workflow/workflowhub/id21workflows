import os
import sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "helpers")))

from workflow_utils import fluoxas  # noqa E402

if __name__ == "__main__":
    fluoxas(
        session="/data/visitor/ma5958/id21/20250207",
        sample="BaCrO4-com-o-aged-thin",
        datasets=["roi107244_125314"],
        scan_ranges=[[2, 3]],
        exclude_scans=[[]],
        config_filenames=["config_nano_6_3.cfg", "config_nano_6_3.cfg"],
        detector_numbers=[0, 1],
        nano=True,
    )
