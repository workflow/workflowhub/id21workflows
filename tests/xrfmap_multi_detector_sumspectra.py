import os
import sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "helpers")))

from workflow_utils import xrfmap  # noqa E402

if __name__ == "__main__":
    xrfmap(
        session="/data/visitor/ls3288/id21/20240221",
        sample="DIV15_10uM_2",
        dataset="roi75698_90925",
        scan_number=1,
        config_filenames=["sample_10200ev_sheldon.cfg"],
        detector_numbers=[0, 1, 2, 3, 4],
        nano=True,
    )
