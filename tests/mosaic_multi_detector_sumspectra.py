import os
import sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "helpers")))

from workflow_utils import mosaic_xrfmap  # noqa E402

if __name__ == "__main__":
    mosaic_xrfmap(
        session="/data/visitor/blc15972/id21/20250128",
        sample="Cd250",
        datasets=["roi106394_124471", "roi106393_124470"],
        scan_ranges=[(1, 1), (1, 1)],
        config_filenames=["config_9800ev.cfg"],
        detector_numbers=[0, 1],
        nano=True,
        resolution={"sy": (1, "um"), "sz": (1, "um")},
    )
