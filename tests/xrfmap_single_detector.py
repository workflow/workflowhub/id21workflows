import os
import sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "helpers")))

from workflow_utils import xrfmap  # noqa E402

if __name__ == "__main__":
    xrfmap(
        session="/data/visitor/hg227/id21/20250121",
        sample="C1_sampleB",
        dataset="roi104537_122450",
        scan_number=1,
        config_filenames=["config_S_batch.cfg"],
        detector_numbers=[0],
        nano=False,
    )
